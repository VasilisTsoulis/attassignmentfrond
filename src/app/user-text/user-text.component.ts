import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-text',
  templateUrl: './user-text.component.html',
  styleUrls: ['./user-text.component.css']
})
export class UserTextComponent implements OnInit {
  textRegister: any;
  userData: { urltext: string };


  constructor(private route: ActivatedRoute, private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userData = { urltext: this.route.snapshot.params['text'] };
    this.route.params
      .subscribe(
        (params: Params) => {
          this.userData = params['text'];
        }
      );
    this.userService.addText({ urltext: this.route.snapshot.params['text'] }).subscribe(
      response => {
        console.log('success');
      },
      error => {
        console.log('error', error);
      }
    );
    setTimeout(() => {
      this.router.navigate(['/users']);
    }, 3000);
  }




}
