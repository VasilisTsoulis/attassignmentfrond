import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { UserTextComponent } from './user-text/user-text.component';



const appRoutes: Routes = [
  { path: '', pathMatch: 'full' , redirectTo: '/users' },
  { path: 'users', component: UsersComponent },
  { path: 'users/:text', component: UserTextComponent }
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
