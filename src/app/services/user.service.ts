import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Text } from '../models/usertext';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  httpHeaders = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});
  baseUrl: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  registerUser(): Observable<any> {
    return this.http.get(this.baseUrl + 'textuser/', this.getAuthHeaders());
  }

  addText(text: Text): Observable<any> {
    return this.http.post(this.baseUrl + 'textuser/', text, this.getAuthHeaders());
  }

  private getAuthHeaders() {
    const httpHeaders = new HttpHeaders(
      {'Content-Type': 'application/json; charset=utf-8'});
    return { headers: httpHeaders};
  }
}
