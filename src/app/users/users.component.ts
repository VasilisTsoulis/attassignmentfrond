import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  getAlltext: any;
  myArray = [];
  longestName: string = '';
  frequentNumber: number;
  frequentLetter = [];

  constructor(private route: ActivatedRoute, private userService: UserService, private router: Router) {
    this.userService.registerUser().subscribe(
      response => {
        this.getAlltext = response;
        this.wordFrequency();
        this.longestword();
        this.commonLetter();
      },
      error => {
        console.log('error', error);
      }
    );

  }

  ngOnInit() {}

  currentName = [];
  wordFrequency() {
    for (let i = 0; i < this.getAlltext.length; i++) {
      this.myArray.push(this.getAlltext[i].urltext);
    }
    this.myArray.sort();
    let current = null;
    let cnt = 0;
    for (let i = 0; i < this.myArray.length; i++) {
      if (this.myArray[i] != current) {
        if (cnt > 0) {
          this.currentName.push({
            current: current,
            cnt: cnt
          });
        }
        current = this.myArray[i];
        cnt = 1;
      } else {
        cnt++;
      }
    }
    if (cnt > 0) {
      this.currentName.push({
        current: current,
        cnt: cnt
      });
    }
  }

  longestword() {
    for (let i = 0; i < this.myArray.length; i++) {
      if (this.myArray[i].length > this.longestName.length) {
        this.longestName = this.myArray[i];
      }
    }
  }

  commonLetter() {
    let exps = {};
    let obj;
    for (let y = 0; y < this.myArray.length; y++) {
      let expCounts = {};
      obj = {};
      let maxKey = '';
      for (let i = 0; i < this.myArray[y].length; i++) {
        let key = this.myArray[y][i];
        if (!expCounts[key]) {
          expCounts[key] = 0;
        }
        expCounts[key]++;
        if (maxKey == '' || expCounts[key] > expCounts[maxKey]) {
          maxKey = key;
        }
      }
      for (let x in expCounts) {
        for (let i = 0; i < Object.keys(expCounts).length; i++) {
          if (!obj[x]) {
            obj[x] = expCounts[x];
          }
        }
        if (exps[x]) {
          exps[x] = (exps[x]) + (obj[x]);
        } else {
          exps[x] = (obj[x]);
        }
      }
    }
    let maxnum = 0;
    let myObj = {};
    let myChar;
    for (let i = 0; i < Object.keys(exps).length; i++) {
      for (let z in exps) {
        if (maxnum < exps[z]) {
          maxnum = exps[z];
          myChar = z;
        }
      }
    }
    this.frequentNumber = maxnum;
    this.frequentLetter = myChar;
  }

}
